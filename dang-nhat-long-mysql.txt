-- 1.	Cho biết danh sách các đối tác cung cấp hàng cho công ty
select * from NHACUNGCAP;
    
-- 2.  Hàng, tên hàng và số lượng của các mặt hàng hiện có trong công ty
select MAHANG, TENHANG, SOLUONG from mathang;

-- 3.	Họ tên, địa chỉ và năm bắt đầu làm việc của các nhân viên trong cty
select CONCAT(HO, ' ',TEN) as HOTEN, DIACHI, YEAR(NGAYLAMVIEC) AS NAMBATDAU from nhanvien;

-- 4.	Địa chỉ, điện thoại của nhà cung cấp có tên giao dịch VINAMILK
select DIACHI, DIENTHOAI,TENGIAODICH 
from nhacungcap
where TENGIAODICH = "VINAMILK";

-- 5.	Mã và tên của các mặt hàng có giá trị lớn hơn 100000 và số lượng hiện có ít hơn 50
select MAHANG, TENHANG from mathang
where GIAHANG > 100000 and SOLUONG < 50;

-- 6.	Cho biết mỗi mặt hàng trong công ty do ai cung cấp
select TENCONGTY, TENHANG
from nhacungcap n inner join mathang m
on n.MACONGTY = m.NHACUNGCAP_MACONGTY;

-- 7.	Công ty Việt Tiến đã cung cấp những mặt hàng nào
select TENCONGTY, TENHANG
from nhacungcap n inner join mathang m
on n.MACONGTY = m.NHACUNGCAP_MACONGTY
where n.TENCONGTY like '%Việt Tiến%';

-- 8.	Loại hàng thực phẩm do những công ty nào cung cấp, địa chỉ của công ty đó
select TENCONGTY, DIACHI
from nhacungcap n inner join mathang m inner join loaihang l
on n.MACONGTY = m.NHACUNGCAP_MACONGTY and m.LOAIHANG_MALOAIHANG = l.MALOAIHANG
where l.TENLOAIHANG = 'thuc pham';

-- 9.	Những khách hàng nào (tên giao dịch) đã đặt mua mặt hàng sữa hộp của công ty
select TENGIAODICH
from khachhang k inner join mathang m inner join chitietdathang c inner join dondathang d
on k.MAKHACHHANG = d.KHACHHANG_MAKHACHHANG 
and d.SOHOADON = c.DONDATHANG_SOHOADON
and c.MATHANG_MAHANG = m.MAHANG
where m.TENHANG = 'sua hop';

-- 10.	Đơn đặt hàng số 1 do ai đặt và do nhân viên nào lập, thời gian và địa điểm giao hàng là ở đâu
select TENCONGTY AS CTYDATHANG, HO, TEN, NGAYGIAOHANG, NOIGIAOHANG
from khachhang k inner join dondathang d inner join nhanvien n 
on k.MAKHACHHANG = d.KHACHHANG_MAKHACHHANG and d.NHANVIEN_MANHANVIEN = n.MANHANVIEN
where d.SOHOADON = 1;

-- 11.	Hãy cho biết số tiền lương mà công ty phải trả cho mỗi nhân viên là bao nhiêu (lương=lương cơ bản+phụ cấp)
select HO, TEN, (LUONGCOBAN + LUONGPHUCAP) AS TIENLUONG
from nhanvien;

-- 12.	Trong đơn đặt hàng số 3 đặt mua những mạt hàng nào và số tiền mà khách hàng phải trả cho mỗi mặt hàng là bao nhiêu
-- 		(số tiền phải trả=số lượng x giá bán – số lượng x giá bán x mức giảm giá/100)
select m.TENHANG, (c.SOLUONG * c.GIABAN -  c.MUCGIAMGIA) AS SOTIENPHAITRA
from mathang m inner join chitietdathang c inner join dondathang d
on m.MAHANG = c.MATHANG_MAHANG and d.SOHOADON = c.DONDATHANG_SOHOADON
where d.SOHOADON = 3;

-- 13.	Hãy cho biết có những khách hàng nào lại chính là đối tác cung cấp hàng cho công ty (tức là có cùng tên giao dịch)
select k.MAKHACHHANG, k.TENCONGTY
from khachhang k inner join dondathang d inner join chitietdathang c inner join mathang m inner join nhacungcap n
on k.MAKHACHHANG = d.KHACHHANG_MAKHACHHANG and d.SOHOADON = c.DONDATHANG_SOHOADON
and c.MATHANG_MAHANG = m.MAHANG and m.NHACUNGCAP_MACONGTY = n.MACONGTY
where n.TENGIAODICH = k.TENGIAODICH
group by k.TENCONGTY;

-- 14.	Trong công ty có những nhân viên nào có cùng ngày sinh
select * from nhanvien
where NGAYSINH in (select NGAYSINH from nhanvien group by NGAYSINH having count(NGAYSINH) > 1 );

-- 15.	Những đơn hàng nào yêu cầu giao hàng ngay tại công ty đặt hàng và những đơn đó là của công ty nào
select  *, k.TENCONGTY from dondathang d inner join khachhang k
on d.KHACHHANG_MAKHACHHANG = k.MAKHACHHANG
where d.NOIGIAOHANG = k.DIACHI;

-- 16.	Cho biết tên công ty, tên giao dịch, địa chỉ và điện thoại của các khách hàng và nhà cung cấp hàng cho công ty
select k.TENCONGTY, k.TENGIAODICH, k.DIACHI, k.DIENTHOAI from khachhang k
union all select n.TENCONGTY, n.TENGIAODICH, n.DIACHI, n.DIENTHOAI from nhacungcap n;

-- 17.	Những mặt hàng nào chưa từng được khách hàng đặt mua
select m.TENHANG, m.MAHANG from mathang m left join chitietdathang c 
on m.MAHANG = c.MATHANG_MAHANG
where m.MAHANG not in (select chitietdathang.MATHANG_MAHANG from chitietdathang);

-- 18.	Những nhân viên nào của công ty chưa từng lập hóa đơn đặt hàng nào
select * from nhanvien n left join dondathang d
on n.MANHANVIEN = d.NHANVIEN_MANHANVIEN
where n.MANHANVIEN not in (select dondathang.NHANVIEN_MANHANVIEN from dondathang);

-- 19.	Những nhân viên nào của công ty có lương cơ bản cao nhất
select * from nhanvien 
where LUONGCOBAN = (select max(LUONGCOBAN) from nhanvien);

-- 20.	Tổng số tiền mà khách hàng phải trả cho mỗi đơn đặt hàng là bao nhiêu
select DONDATHANG_SOHOADON, sum(GIABAN * SOLUONG - MUCGIAMGIA) AS TongTien from chitietdathang
group by DONDATHANG_SOHOADON;

-- 21.	Trong năm 2006 những mặt hàng nào đặt mua đúng mộ lần
select * from mathang m inner join chitietdathang c inner join dondathang d
on m.MAHANG = c.MATHANG_MAHANG and c.DONDATHANG_SOHOADON = d.SOHOADON
where  year(d.NGAYDATHANG) = 2006
group by c.MATHANG_MAHANG
having count(c.MATHANG_MAHANG) = 1  ;

-- 22.	Mỗi khách hàng phải bỏ ra bao nhiêu tiền để đặt mua hàng của công ty
select k.MAKHACHHANG, k.TENCONGTY , sum(GIABAN * SOLUONG - MUCGIAMGIA) AS TongTien 
from chitietdathang c inner join dondathang d inner join khachhang k
on c.DONDATHANG_SOHOADON = d.SOHOADON and k.MAKHACHHANG = d.KHACHHANG_MAKHACHHANG
group by k.MAKHACHHANG;

-- 23.	Mỗi nhân viên của công ty đã lập bao nhiêu đơn đặt hàng (nếu chưa hề lập hóa đơn nào thì cho kết quả là 0)
select n.*, count(d.NHANVIEN_MANHANVIEN) as TongHoaDonDaLap  from nhanvien n left join dondathang d
on n.MANHANVIEN = d.NHANVIEN_MANHANVIEN
group by n.MANHANVIEN;

-- 24.	Tổng số tiền hàng mà công ty thu được trong mỗi tháng của năm 2006 (thời gian được tính theo ngày đặt hàng)
select month(d.NGAYDATHANG) as Thang, sum(c.SOLUONG * c.GIABAN - c.MUCGIAMGIA) as TongTienHang 
from dondathang d inner join chitietdathang c 
where year(d.NGAYDATHANG) = 2006
group by month(d.NGAYDATHANG);

-- 25.	Tông số tiền lời mà công ty thu được từ mỗi mặt hàng trong năm 2006
select m.TENHANG, sum(c.SOLUONG * c.GIABAN - c.MUCGIAMGIA) - sum(m.SOLUONG * m.GIAHANG) as TienLoi
from mathang m inner join chitietdathang c inner join dondathang d
on m.MAHANG = c.MATHANG_MAHANG and d.SOHOADON = c.DONDATHANG_SOHOADON
where year(d.NGAYDATHANG) = 2006
group by (m.MAHANG);

-- 26.	Số lượng hàng còn lại của mỗi mặt hàng mà công ty đã có (tổng số lượng hàng hiện có và đã bán)
select m.TENHANG, m.SOLUONG as TongSoLuong, c.SOLUONG as DaBan, coalesce(m.SOLUONG - c.SOLUONG, m.SOLUONG, c.SOLUONG ) as ConLai
from chitietdathang c right join mathang m 
	on c.MATHANG_MAHANG = m.MAHANG
group by m.MAHANG;

-- 27.	Nhân viên nào của công ty bán được số lượng hàng nhiều nhất và số lượng hàng bán được của mhữmg nhân viên này là bao nhiêu
SELECT n.*, SUM(c.SOLUONG) AS SoLuong
FROM dondathang
INNER JOIN
    chitietdathang c ON dondathang.SOHOADON = c.DONDATHANG_SOHOADON
INNER JOIN nhanvien n on dondathang.NHANVIEN_MANHANVIEN = n.MANHANVIEN
GROUP BY NHANVIEN_MANHANVIEN
HAVING SUM(SOLUONG) >= ALL (SELECT 
        SUM(c.SOLUONG)
    FROM nhanvien n
	INNER JOIN dondathang d
	INNER JOIN chitietdathang c ON d.SOHOADON = c.DONDATHANG_SOHOADON
            AND n.MANHANVIEN = d.NHANVIEN_MANHANVIEN
    GROUP BY n.MANHANVIEN);

-- CÂU 
-- 28.	Đơn đặt hàng nào có số lượng hàng được đặt mua ít nhất
-- 29.	Số tiền nhiều nhất mà khách hàng đã từng bỏ ra để đặt hàng trong các đơn đặt hàng là bao nhiêu
-- 30.	Mỗi một đơn đặt hàng đặt mua những mặt hàng nào và tổng số tiền của đơn đặt hàng
-- 31.	Mỗi một loại hàng bao gồm những mặt hàng nào, 
-- 		tổng số lượng của mỗi loại và tổng số lượng của tất cả các mặt hàng hiện có trong cty
-- 32.	Thông kê trong năm 2006 mỗi một mặt hàng trong mỗi tháng và trong cả năm bán được với số lượng bao nhiêu (Yêu cầu kết quả hiểu thị dưới dạng bảng, hai cột đầu là mã hàng, tên hàng, các cột còn lại tương ứng từ tháng 1 đến tháng 12 và cả năm. Như vậy mỗi dòng trong kết quả cho biết số lượng hàng bán được mỗi tháng và trong cả năm của mỗi mặt hàng


--  Sử dụng câu lệnh UPDATE để thực hiện các yêu cầu
-- 33.	Cập nhật lại giá thị trường NGAYCHUYENHANG của những bản ghi có NGAYCHUYENHANG chưa xác định (NULL) trong bảng DONDATHANG bằng với giá trị của trường NGAYDATHANG
update dondathang
set 
	NGAYCHUYENHANG = NGAYDATHANG
where 
	NGAYCHUYENHANG = null;

-- 34.	Tăng số lượng hàng của những mặt hàng do công ty VINAMILK cung cấp lên gấp đôi
update mathang m inner join nhacungcap n
	on m.NHACUNGCAP_MACONGTY = n.MACONGTY
set 
	m.SOLUONG = m.SOLUONG * 2
where 
	n.TENGIAODICH like '%vinamilk%';

-- 35.Cập nhật giá trị của trường NOIGIAOHANG trong bảng DONDATHANG bằng địa chỉ của khách hàng đối với những đơn đặt hàng chưa xác định được nơi giao hàng (giá trị trường NOIGIAOHANG bằng NULL).
update dondathang d inner join khachhang k
	on d.KHACHHANG_MAKHACHHANG = k.MAKHACHHANG
set d.NOIGIAOHANG = k.DIACHI
where d.NOIGIAOHANG = null;

-- 36.	Cập nhật lại dữ liệu trong bảng KHACHHANG sao cho nếu tên công ty và tên giao dịch của khách hàng trùng với tên công ty và tên giao dịch của một nhà cung cấp nào đó thì địa chỉ, điện thoại, fax và e-mail phải giống nhau.
update khachhang k inner join dondathang d inner join chitietdathang c inner join mathang m inner join nhacungcap n
	on k.MAKHACHHANG = d.KHACHHANG_MAKHACHHANG
	and d.SOHOADON =  c.DONDATHANG_SOHOADON
	and c.MATHANG_MAHANG = m.MAHANG
	and m.NHACUNGCAP_MACONGTY = n.MACONGTY
set 
	k.DIACHI = n.DIACHI,
    k.DIENTHOAI = n.DIENTHOAI,	
    k.EMAIL = n.EMAIL,
    k.FAX = n.FAX
where 
	k.TENCONGTY = n.TENCONGTY and
    k.TENGIAODICH = n.TENGIAODICH;

-- 37. Tăng lương lên gấp rưỡi cho những nhân viên bán được số lượng hàng nhiều hơn 100 trong năm 2003.
update nhanvien n
	inner join (select chitietdathang.DONDATHANG_SOHOADON as SoHoaDon, sum(chitietdathang.SOLUONG) as TongSoLuong from chitietdathang group by chitietdathang.DONDATHANG_SOHOADON) as s 
    inner join dondathang d 
    inner join chitietdathang c
	on n.MANHANVIEN = d.NHANVIEN_MANHANVIEN and 
    d.SOHOADON = c.DONDATHANG_SOHOADON and
	s.SoHoaDon = c.DONDATHANG_SOHOADON
set 
	n.LUONGCOBAN  = n.LUONGCOBAN * 1.5
where 
    year(d.NGAYDATHANG) = 2003 and
    s.TongSoLuong > 100 ;

-- 38. Tăng phụ cấp lên bằng 50% lương cho những nhân viên bán được hàng nhiều nhất.
UPDATE nhanvien 
SET 
    LUONGPHUCAP = lUONGCOBAN / 2
WHERE
    MANHANVIEN = (SELECT 
            s.MANV
        FROM
            ((SELECT 
                NHANVIEN_MANHANVIEN AS MANV
            FROM
                dondathang
            INNER JOIN chitietdathang ON dondathang.SOHOADON = chitietdathang.DONDATHANG_SOHOADON
            GROUP BY NHANVIEN_MANHANVIEN
            HAVING SUM(SOLUONG) >= ALL (
				SELECT 
                    SUM(c.SOLUONG)
                FROM
                    nhanvien n
                INNER JOIN dondathang d
                INNER JOIN chitietdathang c ON d.SOHOADON = c.DONDATHANG_SOHOADON
                    AND n.MANHANVIEN = d.NHANVIEN_MANHANVIEN
                GROUP BY n.MANHANVIEN)))as s ) ;

-- 39. Giảm 25% lương của những nhân viên trong năm 2003 không lập được bất kỳ đơn đặt hàng nào.
update nhanvien left join dondathang 
	on nhanvien.MANHANVIEN = dondathang.NHANVIEN_MANHANVIEN
set 
	nhanvien.LUONGCOBAN = nhanvien.LUONGCOBAN - nhanvien.LUONGCOBAN*0.25 
where year(dondathang.NGAYDATHANG) = 2003 and nhanvien.MANHANVIEN not in (select dondathang.NHANVIEN_MANHANVIEN from dondathang );

-- 40. Giả sử trong bảng DONDATHANG có thêm trường SOTIEN cho biết số tiền mà khách hàng phải trả trong mỗi đơn đặt hàng. Hãy tính giá trị cho trường này.
select d.* , sum(c.GIABAN * c.SOLUONG - c.MUCGIAMGIA) AS SOTIEN 
	from chitietdathang c inner join dondathang d 
	on c.DONDATHANG_SOHOADON = d.SOHOADON
	group by d.SOHOADON;

-- Thực hiện các yêu cầu dưới đây bằng câu lệnh DELETE.
-- 41. Xoá khỏi bảng NHANVIEN những nhân viên đã làm việc trong công ty quá 40 năm.
delete  from nhanvien
where year(now()) - year(nhanvien.NGAYLAMVIEC) > 40;

-- 42. Xoá những đơn đặt hàng trước năm 2000 ra khỏi cơ sở dữ liệu.
delete  from dondathang
where year(dondathang.NGAYDATHANG) < 2000;

-- 43. Xoá khỏi bảng LOAIHANG những loại hàng hiện không có mặt hàng.
delete loaihang
	from loaihang  left join mathang 
	on loaihang.MALOAIHANG = mathang.LOAIHANG_MALOAIHANG
where loaihang.MALOAIHANG not in (select mathang.LOAIHANG_MALOAIHANG from mathang);

-- 44. Xoá khỏi bảng KHACHHANG những khách hàng hiện không có bất kỳ đơn đặt hàng nào cho công ty.
delete khachhang 
	from khachhang left join dondathang
    on khachhang.MAKHACHHANG = dondathang.KHACHHANG_MAKHACHHANG
where khachhang.MAKHACHHANG not in (select dondathang.KHACHHANG_MAKHACHHANG from dondathang);

-- 45. Xoá khỏi bảng MATHANG những mặt hàng có số lượng bằng 0 và không được đặt mua trong bất kỳ đơn đặt hàng nào.
delete mathang
	from mathang left join chitietdathang
	on mathang.MAHANG = chitietdathang.MATHANG_MAHANG
where mathang.SOLUONG = 0 and mathang.LOAIHANG_MALOAIHANG not in (select chitietdathang.MATHANG_MAHANG from chitietdathang);
