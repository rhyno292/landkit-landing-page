let listUser = [{
    "name": "Coral Lotterington",
    "position": "Okocim",
    "office": "Product Management",
    "age": 78,
    "start_date": "11/14/2021",
    "salary": "$3033.66"
}, {
    "name": "Karlotte Helian",
    "position": "Vođinci",
    "office": "Support",
    "age": 46,
    "start_date": "3/30/2022",
    "salary": "$328.78"
}, {
    "name": "Tisha Balstone",
    "position": "Saguday",
    "office": "Sales",
    "age": 66,
    "start_date": "1/18/2022",
    "salary": "$2919.29"
}, {
    "name": "Andrus Kirwin",
    "position": "Jīwani",
    "office": "Training",
    "age": 29,
    "start_date": "9/2/2021",
    "salary": "$3848.79"
}, {
    "name": "Bryana Klezmski",
    "position": "Huicungo",
    "office": "Support",
    "age": 19,
    "start_date": "2/23/2022",
    "salary": "$2263.04"
}, {
    "name": "Ahmad Nendick",
    "position": "Lipník nad Bečvou",
    "office": "Services",
    "age": 52,
    "start_date": "12/25/2021",
    "salary": "$6848.57"
}, {
    "name": "Therese Semaine",
    "position": "Dortmund",
    "office": "Training",
    "age": 74,
    "start_date": "1/8/2022",
    "salary": "$7040.81"
}, {
    "name": "Charlton Theobold",
    "position": "Ojrzeń",
    "office": "Product Management",
    "age": 29,
    "start_date": "6/26/2021",
    "salary": "$4802.41"
}, {
    "name": "Holly Losano",
    "position": "Sinuknipan",
    "office": "Services",
    "age": 67,
    "start_date": "2/21/2022",
    "salary": "$9279.35"
}, {
    "name": "Bryn Polack",
    "position": "Chicago",
    "office": "Training",
    "age": 94,
    "start_date": "7/20/2021",
    "salary": "$8295.68"
}, {
    "name": "Thaine Locket",
    "position": "Yishi",
    "office": "Training",
    "age": 5,
    "start_date": "1/20/2022",
    "salary": "$3736.16"
}, {
    "name": "Hazel Mertel",
    "position": "Panoongan",
    "office": "Support",
    "age": 11,
    "start_date": "10/20/2021",
    "salary": "$7459.02"
}, {
    "name": "Analiese Raddish",
    "position": "Jendouba",
    "office": "Accounting",
    "age": 48,
    "start_date": "4/20/2022",
    "salary": "$7337.21"
}, {
    "name": "Rania Dyble",
    "position": "Brampton",
    "office": "Training",
    "age": 57,
    "start_date": "5/24/2022",
    "salary": "$6273.02"
}, {
    "name": "Vlad Wapplington",
    "position": "Vilhena",
    "office": "Business Development",
    "age": 40,
    "start_date": "3/1/2022",
    "salary": "$9621.44"
}, {
    "name": "Krispin Verdy",
    "position": "Néa Ionía",
    "office": "Legal",
    "age": 15,
    "start_date": "4/18/2022",
    "salary": "$8157.73"
}, {
    "name": "Steffen Elesander",
    "position": "Caper",
    "office": "Services",
    "age": 57,
    "start_date": "11/13/2021",
    "salary": "$9660.49"
}, {
    "name": "Lawrence Ladbrooke",
    "position": "Aachen",
    "office": "Research and Development",
    "age": 6,
    "start_date": "3/4/2022",
    "salary": "$7607.85"
}, {
    "name": "Mikol Tobin",
    "position": "Tysmenychany",
    "office": "Training",
    "age": 99,
    "start_date": "6/22/2021",
    "salary": "$8150.62"
}, {
    "name": "Rory Doddemeade",
    "position": "Sheksna",
    "office": "Engineering",
    "age": 69,
    "start_date": "5/15/2022",
    "salary": "$1106.76"
}, {
    "name": "Tony Josupeit",
    "position": "Bancal",
    "office": "Business Development",
    "age": 15,
    "start_date": "4/11/2022",
    "salary": "$6861.19"
}, {
    "name": "Rickey Knolles-Green",
    "position": "Gulao",
    "office": "Marketing",
    "age": 75,
    "start_date": "1/11/2022",
    "salary": "$6822.96"
}, {
    "name": "Maribeth Larkcum",
    "position": "Portëz",
    "office": "Research and Development",
    "age": 3,
    "start_date": "12/3/2021",
    "salary": "$8951.63"
}, {
    "name": "Gianina Sussex",
    "position": "Reshetnikovo",
    "office": "Business Development",
    "age": 36,
    "start_date": "6/26/2021",
    "salary": "$3301.75"
}, {
    "name": "Stavros Jumonet",
    "position": "Tigpalay",
    "office": "Services",
    "age": 32,
    "start_date": "12/17/2021",
    "salary": "$666.37"
}, {
    "name": "Irena Snawden",
    "position": "Parychy",
    "office": "Support",
    "age": 6,
    "start_date": "2/5/2022",
    "salary": "$1917.92"
}, {
    "name": "Elenore Link",
    "position": "Jiufang",
    "office": "Sales",
    "age": 53,
    "start_date": "1/5/2022",
    "salary": "$7846.57"
}, {
    "name": "Mario Possek",
    "position": "Winong",
    "office": "Product Management",
    "age": 57,
    "start_date": "7/8/2021",
    "salary": "$6441.71"
}, {
    "name": "Dulce Speller",
    "position": "Jindřichův Hradec",
    "office": "Accounting",
    "age": 87,
    "start_date": "4/4/2022",
    "salary": "$885.00"
}, {
    "name": "Wilhelmina Dreus",
    "position": "Nantes",
    "office": "Training",
    "age": 99,
    "start_date": "9/12/2021",
    "salary": "$6686.67"
}, {
    "name": "Katheryn Matasov",
    "position": "Stockholm",
    "office": "Training",
    "age": 17,
    "start_date": "8/10/2021",
    "salary": "$1710.23"
}, {
    "name": "Dionisio Tremonte",
    "position": "Teroual",
    "office": "Product Management",
    "age": 88,
    "start_date": "8/29/2021",
    "salary": "$6245.52"
}, {
    "name": "Maje Maciocia",
    "position": "Basco",
    "office": "Engineering",
    "age": 69,
    "start_date": "3/10/2022",
    "salary": "$3413.51"
}, {
    "name": "Niko Glasson",
    "position": "Mestre",
    "office": "Marketing",
    "age": 7,
    "start_date": "10/17/2021",
    "salary": "$6607.04"
}, {
    "name": "Hatti Everard",
    "position": "Bururi",
    "office": "Engineering",
    "age": 61,
    "start_date": "10/15/2021",
    "salary": "$6569.85"
}, {
    "name": "Horst Keyzman",
    "position": "Blagoveshchenka",
    "office": "Business Development",
    "age": 41,
    "start_date": "10/5/2021",
    "salary": "$939.13"
}, {
    "name": "Cacilia McVeighty",
    "position": "Kangaslampi",
    "office": "Sales",
    "age": 74,
    "start_date": "8/10/2021",
    "salary": "$3869.23"
}, {
    "name": "Nerita Woodger",
    "position": "Mateur",
    "office": "Engineering",
    "age": 38,
    "start_date": "6/3/2022",
    "salary": "$863.53"
}, {
    "name": "Jaimie Gebby",
    "position": "Erdaocha",
    "office": "Research and Development",
    "age": 3,
    "start_date": "9/12/2021",
    "salary": "$9056.45"
}, {
    "name": "Barbra Manilo",
    "position": "Nanganumba",
    "office": "Sales",
    "age": 72,
    "start_date": "1/4/2022",
    "salary": "$2856.21"
}, {
    "name": "Lesley Laban",
    "position": "Dob",
    "office": "Legal",
    "age": 19,
    "start_date": "9/27/2021",
    "salary": "$3035.22"
}, {
    "name": "Emerson Jeanesson",
    "position": "Ikar",
    "office": "Research and Development",
    "age": 67,
    "start_date": "2/24/2022",
    "salary": "$6144.83"
}, {
    "name": "Mariana Patience",
    "position": "Psyzh",
    "office": "Training",
    "age": 10,
    "start_date": "8/28/2021",
    "salary": "$5064.80"
}, {
    "name": "Derrek Tyt",
    "position": "Ruchihe",
    "office": "Research and Development",
    "age": 39,
    "start_date": "7/21/2021",
    "salary": "$3044.52"
}, {
    "name": "Kittie Camerati",
    "position": "Kangshan",
    "office": "Support",
    "age": 53,
    "start_date": "3/25/2022",
    "salary": "$7214.23"
}, {
    "name": "Lynna Wattisham",
    "position": "Bosaso",
    "office": "Accounting",
    "age": 43,
    "start_date": "6/5/2022",
    "salary": "$8939.76"
}, {
    "name": "Ardelia Brabham",
    "position": "Dniprodzerzhyns’k",
    "office": "Sales",
    "age": 62,
    "start_date": "2/12/2022",
    "salary": "$5773.15"
}, {
    "name": "Lonnie Ferier",
    "position": "Miami",
    "office": "Services",
    "age": 67,
    "start_date": "8/15/2021",
    "salary": "$8833.48"
}, {
    "name": "Davida Musgrave",
    "position": "Panao",
    "office": "Legal",
    "age": 59,
    "start_date": "12/30/2021",
    "salary": "$2314.40"
}, {
    "name": "Dimitry Lievesley",
    "position": "Ārifwāla",
    "office": "Research and Development",
    "age": 97,
    "start_date": "11/26/2021",
    "salary": "$3297.54"
}, {
    "name": "Norrie Andreev",
    "position": "Río Blanquito",
    "office": "Engineering",
    "age": 98,
    "start_date": "6/15/2021",
    "salary": "$2032.42"
}, {
    "name": "Cordelie Vasyunkin",
    "position": "Aguachica",
    "office": "Services",
    "age": 22,
    "start_date": "6/10/2022",
    "salary": "$6506.38"
}, {
    "name": "Anita Kindred",
    "position": "Haugesund",
    "office": "Business Development",
    "age": 76,
    "start_date": "1/3/2022",
    "salary": "$4938.69"
}, {
    "name": "Ruthanne Constantinou",
    "position": "Sukamaju",
    "office": "Services",
    "age": 9,
    "start_date": "8/18/2021",
    "salary": "$5480.48"
}, {
    "name": "Fax Gerbi",
    "position": "Solntsevo",
    "office": "Marketing",
    "age": 86,
    "start_date": "9/18/2021",
    "salary": "$5477.88"
}, {
    "name": "Gus Polsin",
    "position": "Buzet",
    "office": "Support",
    "age": 66,
    "start_date": "5/14/2022",
    "salary": "$3225.03"
}, {
    "name": "Hagan Heaysman",
    "position": "Wolomoni",
    "office": "Marketing",
    "age": 1,
    "start_date": "4/30/2022",
    "salary": "$7087.39"
}, {
    "name": "Sharon Hugonneau",
    "position": "Phayuha Khiri",
    "office": "Engineering",
    "age": 62,
    "start_date": "9/15/2021",
    "salary": "$7201.09"
}, {
    "name": "Muire Walenta",
    "position": "Ash Shāmīyah",
    "office": "Product Management",
    "age": 35,
    "start_date": "4/25/2022",
    "salary": "$7026.20"
}, {
    "name": "Chaddy Sercombe",
    "position": "Sabon Gari-Nangere",
    "office": "Services",
    "age": 43,
    "start_date": "6/5/2022",
    "salary": "$736.15"
}, {
    "name": "Aluino Magenny",
    "position": "Seara",
    "office": "Sales",
    "age": 47,
    "start_date": "2/15/2022",
    "salary": "$6973.88"
}, {
    "name": "Fayette Dulton",
    "position": "Dangchang Chengguanzhen",
    "office": "Product Management",
    "age": 19,
    "start_date": "12/19/2021",
    "salary": "$8588.33"
}, {
    "name": "Theodora Stowe",
    "position": "Huanchaco",
    "office": "Business Development",
    "age": 70,
    "start_date": "12/15/2021",
    "salary": "$4905.14"
}, {
    "name": "Wylie Viger",
    "position": "Jinji",
    "office": "Training",
    "age": 42,
    "start_date": "5/11/2022",
    "salary": "$1287.35"
}, {
    "name": "Yuri Ongin",
    "position": "Gamleby",
    "office": "Accounting",
    "age": 93,
    "start_date": "11/9/2021",
    "salary": "$6966.71"
}, {
    "name": "Reeta Reignould",
    "position": "Zákupy",
    "office": "Business Development",
    "age": 61,
    "start_date": "12/17/2021",
    "salary": "$5713.23"
}, {
    "name": "Gallard Ivanishin",
    "position": "Óbidos",
    "office": "Sales",
    "age": 34,
    "start_date": "11/6/2021",
    "salary": "$2651.27"
}, {
    "name": "Tatiania Terbeck",
    "position": "Banān",
    "office": "Marketing",
    "age": 58,
    "start_date": "10/4/2021",
    "salary": "$4663.06"
}, {
    "name": "Kakalina Verryan",
    "position": "Oljoq",
    "office": "Engineering",
    "age": 15,
    "start_date": "4/18/2022",
    "salary": "$6955.96"
}, {
    "name": "Aeriell Goose",
    "position": "Toyama-shi",
    "office": "Product Management",
    "age": 45,
    "start_date": "8/5/2021",
    "salary": "$0.79"
}, {
    "name": "Noami Stranger",
    "position": "Kasembon",
    "office": "Human Resources",
    "age": 65,
    "start_date": "8/25/2021",
    "salary": "$92.79"
}, {
    "name": "Saunder Mathews",
    "position": "Zhenziliang",
    "office": "Accounting",
    "age": 64,
    "start_date": "10/2/2021",
    "salary": "$9280.44"
}, {
    "name": "Arlette Roll",
    "position": "Rialma",
    "office": "Sales",
    "age": 6,
    "start_date": "11/25/2021",
    "salary": "$243.48"
}, {
    "name": "Alberik Hallawell",
    "position": "Bagamoyo",
    "office": "Services",
    "age": 68,
    "start_date": "11/15/2021",
    "salary": "$7818.18"
}, {
    "name": "Ewart Schops",
    "position": "Shuangwang",
    "office": "Business Development",
    "age": 3,
    "start_date": "6/1/2022",
    "salary": "$7562.79"
}, {
    "name": "Taryn Bliss",
    "position": "Gande",
    "office": "Accounting",
    "age": 24,
    "start_date": "7/17/2021",
    "salary": "$2387.77"
}, {
    "name": "Barbey Ludlem",
    "position": "Taozhuang",
    "office": "Legal",
    "age": 68,
    "start_date": "11/8/2021",
    "salary": "$7993.78"
}, {
    "name": "Alyda Schwand",
    "position": "Yangjia",
    "office": "Support",
    "age": 73,
    "start_date": "10/8/2021",
    "salary": "$3651.01"
}]

const tbody = document.getElementById('tbody')
const numberOfEntry = document.getElementById('numberOfEntry')
const entryDescription = document.querySelector('.table__footer__left')
const searchQuery = document.getElementById('searchQuery')

var currentPage;

// Render List Data
function renderList(data, pageNumber, entryPerPage) {
    let listToShow = [...data].splice(pageNumber * entryPerPage, entryPerPage)
    currentPage = pageNumber;
    let innerHTML;
    if (listToShow.length === 0) {
        innerHTML = `<td colspan="5">Not found record with "${searchQuery.value}"</td>`
        tbody.innerHTML = innerHTML;
        setEntryDescription(0, 0, listUser.length)
        showPagination([])
    }
    else {
        innerHTML = listToShow.map((item, index) => {
            return `<tr>
                <td>${item.name}</td>
                <td>${item.position}</td>
                <td>${item.office}</td>
                <td>${item.age}</td>
                <td>${item.start_date}</td>
            </tr>`
        })
        tbody.innerHTML = innerHTML.join('');
        setEntryDescription(pageNumber * entryPerPage + 1, (entryPerPage * pageNumber + entryPerPage), listUser.length)
        showPagination(data)
    }
}

// Show description ex: "show 1 to 24 entries of 100 entries"
function setEntryDescription(from, to, total) {
    entryDescription.innerHTML = `<div style="color:#4338ca">Show ${from} to ${to > total ? total : to} entries of ${total} entries</div>`
}

// Render Listdata by selected entry 10 25 50 100
function showEntry() {
    const searchQueryFormat = searchQuery.value.toLowerCase();
    if (searchQueryFormat.length !== 0) return
    else {
        console.log(Number(numberOfEntry.value))
        renderList(listUser, 0, Number(numberOfEntry.value))
        setEntryDescription(1, Number(numberOfEntry.value), listUser.length)
    }
}


// Search
function search() {

    let searchResult;
    const searchQueryFormat = searchQuery.value.toLowerCase();
    searchResult = listUser.filter((item, index) => {
        const { name, position, office, age } = item;
        return name.toLowerCase().includes(searchQueryFormat)
            || position.toLowerCase().includes(searchQueryFormat)
            || office.toLowerCase().includes(searchQueryFormat)
            || age.toString().toLowerCase().includes(searchQueryFormat)
    })
    if (searchQuery !== "") {
        renderList(searchResult, 0, searchResult.length)
        showPagination(searchResult)
        setEntryDescription(1, searchResult.length, listUser.length)
    }
}

// Sort Name
const listTh = document.getElementsByTagName("th")
function sort(param) {
    let newList = [...listUser];
    for (let i = 0; i < listTh.length; i++) {
        if (listTh[i].className.includes('sort__name-asc')) {
            newList.sort((a, b) => {
                let propA, propB;
                if (param === 'age') {
                    propA = a.age;
                    propB = b.age;
                }
                else {
                    propA = a[`${param}`].toUpperCase()
                    propB = b[`${param}`].toUpperCase()
                }
                listTh[i].classList.remove('sort__name-asc')
                listTh[i].classList.add('sort__name-desc')
                return propA > propB ? 1 : propA === propB ? 0 : -1
            })
        }
        else {
            newList.sort((a, b) => {
                let propA, propB;
                if (param === 'age') {
                    propA = a.age;
                    propB = b.age;
                }
                else {
                    propA = a[`${param}`].toUpperCase()
                    propB = b[`${param}`].toUpperCase()
                }
                listTh[i].classList.remove('sort__name-desc')
                listTh[i].classList.add('sort__name-asc')
                return propA < propB ? 1 : propA === propB ? 0 : -1
            })
        }
    }

    listUser = newList;
    renderList(newList, 0, Number(numberOfEntry.value))
}

// Show button pagination
function showPagination(listUser) {
    let view = document.querySelector('.table__footer__pagination__number')
    let totalPage = Math.ceil(listUser.length / Number(numberOfEntry.value))
    let innerHTML = '';
    for (let i = 0; i < totalPage; i++) {
        innerHTML = innerHTML.concat(
            `<button 
                style="border-radius: 1rem;background-color: white;padding: 0.5rem 1rem;font-weight:bold; cursor:pointer; border:rgb(229 231 235) 1px solid; ${currentPage === i ? "color:#4338ca;transform: scale(1.3); background-color: #4338ca; color:white;border:none" : "color: #4338ca;"}"
                onclick="renderList(listUser,${i},${Number(numberOfEntry.value)})">
                ${i + 1}
            </button>`)
    }
    view.innerHTML = innerHTML
}

function gotoPrevious() {

    if (currentPage === 0) {
        return
    };
    currentPage -= 1;
    renderList(listUser, currentPage, Number(numberOfEntry.value))
}

function gotoNext() {
    if (currentPage === Math.ceil(listUser.length / Number(numberOfEntry.value)) - 1) return;
    currentPage += 1;
    renderList(listUser, currentPage, Number(numberOfEntry.value))
}

document.addEventListener('keydown', function (e) {
    if (e.code === 'ArrowRight' || e.code === 'ArrowUp') {
        gotoNext();
    } else if (e.code === 'ArrowLeft' || e.code === 'ArrowDown') {
        gotoPrevious();
    }
});

// Init
setEntryDescription(currentPage * Number(numberOfEntry.value) + 1, (Number(numberOfEntry.value) * (currentPage <= 0 ? 1 : currentPage) + Number(numberOfEntry.value)), listUser.length)
renderList(listUser, 0, 10)
